let slideIndex = 0;
const gallery = document.querySelector('.gallery');
const slides = document.querySelectorAll('.gallery-item');

function changeSlide(direction) {
    slideIndex = (slideIndex + direction + slides.length) % slides.length;
    const offset = -slideIndex * 100; // Assuming each slide is 100% width
    gallery.style.transform = `translateX(${offset}%)`;
}

setInterval(() => changeSlide(1), 5000);
